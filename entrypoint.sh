#!/bin/sh

#any line fails return error
set -e

#substitute environment variables from default.comf.tpl and write them into default.conf inside the container
envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf

#start NGINX and make sure this does not run as background daemon - docker containers apps should run in foreground of docker process
nginx -g 'daemon off;'